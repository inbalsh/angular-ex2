import { Component, OnInit, Input } from '@angular/core';

@Component({
  selector: 'list-item',
  templateUrl: './list-item.component.html',
  styleUrls: ['./list-item.component.css']
})
export class ListItemComponent implements OnInit {

  
  @Input() data:any; // הגדרת אינפוט עם תכונה דטה

  id;
  title;
  price;
  stock;
  redcolor=false; // משתנה בוליאני להגדרת הצבע
 
   
  constructor() { }

  ngOnInit() {
      // בעת יצירת אלמנט ליסט חדש, הפונקציה הזו רצה
      if (this.data.stock<10) { 
        this.redcolor = true;
     }
      this.id = this.data.id;
      this.title = this.data.title;
      this.price = this.data.price;
      this.stock = this.data.stock;
  }

}
